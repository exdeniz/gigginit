$(document).ready(function() {
	$('.slider').slick({
		dots: true,
		autoplay: true,
		autoplaySpeed: 10000

	});

	$('.scrollTo').click(function() {
		var idscroll = $(this).attr('href');
		$.scrollTo(idscroll, 800, {
			offset: -75
		});
		return false;

	});
	$('input').iCheck();
	$('.datepicker').pickadate({
		format: 'dd/mm/yyyy',
		min: true
	});
	$('.timepicker').pickatime({
		interval: 60,
		min: [7, 30],
		max: [21,30]
	});
});

// function slider() {
//   var slides =  [
//     ".howToWorkTextItemOne",
//     ".howToWorkTextItemTwo",
//     ".howToWorkTextItemThree"
//    ];

//    slides.forEach(function(entry) {
//         window.setTimeout(function() {
//            var i = i + 1
//            $('.howToWork').css('background-image','url("../img/slideBG-' + i +
//             '.jpg")')
//            $('.howToWorkText').children('div').removeClass('howToWorkTextActive')
//            $(item).addClass('howToWorkTextActive')
//            $('.howToWorkImage img').attr('src','img/hiring_0' + i +
//             '.png')
//         }, 1000);
// }
// );

/**
 * jQuery alterClass plugin
 *
 * Remove element classes with wildcard matching. Optionally add classes:
 *   $( '#foo' ).alterClass( 'foo-* bar-*', 'foobar' )
 *
 * Copyright (c) 2011 Pete Boere (the-echoplex.net)
 * Free under terms of the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 */
(function($) {

	$.fn.alterClass = function(removals, additions) {

		var self = this;

		if (removals.indexOf('*') === -1) {
			// Use native jQuery methods if there is no wildcard matching
			self.removeClass(removals);
			return !additions ? self : self.addClass(additions);
		}

		var patt = new RegExp('\\s' +
			removals.replace(/\*/g, '[A-Za-z0-9-_]+').split(' ').join('\\s|\\s') +
			'\\s', 'g');

		self.each(function(i, it) {
			var cn = ' ' + it.className + ' ';
			while (patt.test(cn)) {
				cn = cn.replace(patt, ' ');
			}
			it.className = $.trim(cn);
		});

		return !additions ? self : self.addClass(additions);
	};

})(jQuery);

$(".howToWorkTextItemOne").click(function() {
	//$('.howToWork').css('background-image','url("../img/slideBG-1.jpg")')
	$('.howToWork').alterClass('howToWorkSlide*');
	$('.howToWork').addClass('howToWorkSlideOneBG');
	$('.howToWorkText').children('div').removeClass('howToWorkTextActive');
	$(this).addClass('howToWorkTextActive');
	$('.howToWorkImage img').attr('src', 'img/hiring_01.png');
});

$(".howToWorkTextItemTwo").click(function() {
	// $('.howToWork').css('background-image','url("../img/slideBG-2.jpg")')
	$('.howToWork').alterClass('howToWorkSlide*');
	$('.howToWork').addClass('howToWorkSlideTwoBG');
	$('.howToWorkText').children('div').removeClass('howToWorkTextActive');
	$(this).addClass('howToWorkTextActive');
	$('.howToWorkImage img').attr('src', 'img/hiring_02.png');
});

$(".howToWorkTextItemThree").click(function() {
	// $('.howToWork').css('background-image','url("../img/slideBG-3.jpg")')
	$('.howToWork').alterClass('howToWorkSlide*');
	$('.howToWork').addClass('howToWorkSlideThreeBG');
	$('.howToWorkText').children('div').removeClass('howToWorkTextActive');
	$(this).addClass('howToWorkTextActive');
	$('.howToWorkImage img').attr('src', 'img/hiring_03.png');
});

var input = document.getElementById("awesomplete");
var awesomplete = new Awesomplete(input);

awesomplete.list = [
	'New York, New York', 'Los Angeles, California', 'Chicago, Illinois', 'Houston, Texas', 'Philadelphia, Pennsylvania', 'Phoenix, Arizona', 'San Antonio, Texas', 'San Diego, California', 'Dallas, Texas', 'San Jose, California', 'Jacksonville, Florida', 'Indianapolis, Indiana', 'San Francisco, California', 'Austin, Texas', 'Columbus, Ohio', 'Fort Worth, Texas', 'Charlotte, North Carolina', 'Detroit, Michigan', 'El Paso, Texas', 'Memphis, Tennessee', 'Baltimore, Maryland', 'Boston, Massachusetts', 'Seattle, Washington', 'Washington, District of Columbia', 'Nashville, Tennessee', 'Denver, Colorado', 'Louisville, Kentucky', 'Milwaukee, Wisconsin', 'Portland, Oregon', 'Las Vegas, Nevada', 'Oklahoma City, Oklahoma', 'Albuquerque, New Mexico', 'Tucson, Arizona', 'Fresno, California', 'Sacramento, California', 'Long Beach, California', 'Kansas City, Missouri', 'Mesa, Arizona', 'Virginia Beach, Virginia', 'Atlanta, Georgia', 'Colorado Springs, Colorado', 'Omaha, Nebraska', 'Raleigh, North Carolina', 'Miami, Florida', 'Cleveland, Ohio', 'Tulsa, Oklahoma', 'Oakland, California', 'Minneapolis, Minnesota', 'Wichita, Kansas', 'Arlington, Texas', 'Bakersfield, California', 'New Orleans, Louisiana', 'Honolulu, Hawaii', 'Anaheim, California', 'Tampa, Florida', 'Aurora, Colorado', 'Santa Ana, California', 'Saint Louis, Missouri', 'Pittsburgh, Pennsylvania', 'Corpus Christi, Texas', 'Riverside, California', 'Cincinnati, Ohio', 'Lexington, Kentucky', 'Anchorage, Alaska', 'Stockton, California', 'Toledo, Ohio', 'Saint Paul, Minnesota', 'Newark, New Jersey', 'Greensboro, North Carolina', 'Buffalo, New York', 'Plano, Texas', 'Lincoln, Nebraska', 'Henderson, Nevada', 'Fort Wayne, Indiana', 'Jersey City, New Jersey', 'Saint Petersburg, Florida', 'Chula Vista, California', 'Norfolk, Virginia', 'Orlando, Florida', 'Chandler, Arizona', 'Laredo, Texas', 'Madison, Wisconsin', 'Winston-Salem, North Carolina', 'Lubbock, Texas', 'Baton Rouge, Louisiana', 'Durham, North Carolina', 'Garland, Texas', 'Glendale, Arizona', 'Reno, Nevada', 'Hialeah, Florida', 'Chesapeake, Virginia', 'Scottsdale, Arizona', 'North Las Vegas, Nevada', 'Irving, Texas', 'Fremont, California', 'Irvine, California', 'Birmingham, Alabama', 'Rochester, New York', 'San Bernardino, California', 'Spokane, Washington', 'Gilbert, Arizona', 'Arlington, Virginia', 'Montgomery, Alabama', 'Boise, Idaho', 'Richmond, Virginia', 'Des Moines, Iowa', 'Modesto, California', 'Fayetteville, North Carolina', 'Shreveport, Louisiana', 'Akron, Ohio', 'Tacoma, Washington', 'Aurora, Illinois', 'Oxnard, California', 'Fontana, California', 'Yonkers, New York', 'Augusta, Georgia', 'Mobile, Alabama', 'Little Rock, Arkansas', 'Moreno Valley, California', 'Glendale, California', 'Amarillo, Texas', 'Huntington Beach, California', 'Columbus, Georgia', 'Grand Rapids, Michigan', 'Salt Lake City, Utah', 'Tallahassee, Florida', 'Worcester, Massachusetts', 'Newport News, Virginia', 'Huntsville, Alabama', 'Knoxville, Tennessee', 'Providence, Rhode Island', 'Santa Clarita, California', 'Grand Prairie, Texas', 'Brownsville, Texas', 'Jackson, Mississippi', 'Overland Park, Kansas', 'Garden Grove, California', 'Santa Rosa, California', 'Chattanooga, Tennessee', 'Oceanside, California', 'Fort Lauderdale, Florida', 'Rancho Cucamonga, California', 'Port Saint Lucie, Florida', 'Ontario, California', 'Vancouver, Washington', 'Tempe, Arizona', 'Springfield, Missouri', 'Lancaster, California', 'Eugene, Oregon', 'Pembroke Pines, Florida', 'Salem, Oregon', 'Cape Coral, Florida', 'Peoria, Arizona', 'Sioux Falls, South Dakota', 'Springfield, Massachusetts', 'Elk Grove, California', 'Rockford, Illinois', 'Palmdale, California', 'Corona, California', 'Salinas, California', 'Pomona, California', 'Pasadena, Texas', 'Joliet, Illinois', 'Paterson, New Jersey', 'Kansas City, Kansas', 'Torrance, California', 'Syracuse, New York', 'Bridgeport, Connecticut', 'Hayward, California', 'Fort Collins, Colorado', 'Escondido, California', 'Lakewood, Colorado', 'Naperville, Illinois', 'Dayton, Ohio', 'Hollywood, Florida', 'Sunnyvale, California', 'Alexandria, Virginia', 'Mesquite, Texas', 'Hampton, Virginia', 'Pasadena, California', 'Orange, California', 'Savannah, Georgia', 'Cary, North Carolina', 'Fullerton, California', 'Warren, Michigan', 'Clarksville, Tennessee', 'McKinney, Texas', 'McAllen, Texas', 'New Haven, Connecticut', 'Sterling Heights, Michigan', 'West Valley City, Utah', 'Columbia, South Carolina', 'Killeen, Texas', 'Topeka, Kansas', 'Thousand Oaks, California', 'Cedar Rapids, Iowa', 'Olathe, Kansas', 'Elizabeth, New Jersey', 'Waco, Texas', 'Hartford, Connecticut', 'Visalia, California', 'Gainesville, Florida', 'Simi Valley, California', 'Stamford, Connecticut', 'Bellevue, Washington', 'Concord, California', 'Miramar, Florida', 'Coral Springs, Florida', 'Lafayette, Louisiana', 'Charleston, South Carolina', 'Carrollton, Texas', 'Roseville, California', 'Thornton, Colorado', 'Beaumont, Texas', 'Allentown, Pennsylvania', 'Surprise, Arizona', 'Evansville, Indiana', 'Abilene, Texas', 'Frisco, Texas', 'Independence, Missouri', 'Santa Clara, California', 'Springfield, Illinois', 'Vallejo, California', 'Victorville, California', 'Athens, Georgia', 'Peoria, Illinois', 'Lansing, Michigan', 'Ann Arbor, Michigan', 'El Monte, California', 'Denton, Texas', 'Berkeley, California', 'Provo, Utah', 'Downey, California', 'Midland, Texas', 'Norman, Oklahoma', 'Waterbury, Connecticut', 'Costa Mesa, California', 'Inglewood, California', 'Manchester, New Hampshire', 'Murfreesboro, Tennessee', 'Columbia, Missouri', 'Elgin, Illinois', 'Clearwater, Florida', 'Miami Gardens, Florida', 'Rochester, Minnesota', 'Pueblo, Colorado', 'Lowell, Massachusetts', 'Wilmington, North Carolina', 'San Buenaventura (Ventura), California', 'Arvada, Colorado', 'Westminster, Colorado', 'West Covina, California', 'Gresham, Oregon', 'Norwalk, California', 'Fargo, North Dakota', 'Carlsbad, California', 'Fairfield, California', 'Cambridge, Massachusetts', 'Wichita Falls, Texas', 'High Point, North Carolina', 'Billings, Montana', 'Green Bay, Wisconsin', 'West Jordan, Utah', 'Richmond, California', 'Murrieta, California', 'Burbank, California', 'Palm Bay, Florida', 'Everett, Washington', 'Flint, Michigan', 'Antioch, California', 'Erie, Pennsylvania', 'South Bend, Indiana', 'Daly City, California', 'Centennial, Colorado', 'Temecula, California'
];
