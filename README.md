HTML and CSS coding here

###Сервер s

Установка локального express and jade

```bash
npm install --production
```
Запуск сервера

```bash
node app.js
```

Сервер доступен по адресу:


http://localhost:3001

### Разработка

Установка среды разработки

```bash
npm install
```
Запуск сервера разработки

```bash
gulp
```

Сервер доступен на 


http://localhost:3000 

Страница после правки исходных файлов обновляется автоматически.


Сборка html файлов

```bash
gulp jadehtml
```

Собирает html файлы в public из jade template